
#include "custom_scene.hpp"


#ifdef CUSTOM_SCENE


using namespace vcl;

/* CLOTH */


/* END CLOTH */


/** Function returning the index i such that t \in [v[i].t,v[i+1].t] */
static size_t index_at_value(float t, const vcl::buffer<vec3t> &v);

static vec3 linear_interpolation(float t, float t1, float t2, const vec3& p1, const vec3& p2);

vec3 cardinal_spline_interpolation(float t, float t0, float t1, float t2, float t3, const vec3& p0, const vec3& p1, const vec3& p2, const vec3& p3);

void scene_model::setup_data(std::map<std::string, GLuint>& shaders,
        scene_structure& scene, gui_structure& gui)
{
    const float radius_body = 0.25f;
    const float radius_head = 0.18f;
    const float radius_arm = 0.05f;
    const float length_arm = 0.2f;

    // The geometry of the body is a sphere
    mesh_drawable body = mesh_drawable(mesh_primitive_sphere(radius_body, { 0,0,0 }, 40, 40));
    mesh_drawable head = mesh_drawable(mesh_primitive_sphere(radius_head, { 0,0,0 }, 40, 40));

    // Geometry of the eyes: black spheres
    mesh_drawable eye = mesh_drawable(mesh_primitive_sphere(0.05f, { 0,0,0 }, 20, 20));
    mesh_drawable beak = mesh_drawable(mesh_primitive_cone(0.08f, { 0,0,0 }, { 0,0,0.25f }));
    eye.uniform.color = { 0,0,0 };
    beak.uniform.color = { 1.0f, 0.8f, 0.2f };

    mesh grip = mesh_primitive_sphere(0.0f, { 0,0,0 }, 20, 20);

    // Shoulder part and arm are displayed as cylinder
    mesh_drawable shoulder = mesh_primitive_quad({ 0.0f,0.0f,-0.15f }, { -0.2f,0.0f,-0.15f }, { -0.2f,0.0f,0.15f }, { 0.0f,0.0f,0.15f });
    mesh_drawable arm = mesh_primitive_quad({ 0.0,0.0f,0.15f }, { 0.0,0.0f,-0.15f }, { -0.3f,0.0f,-0.10 }, { -0.3f,0.0f,0.10f });
    // An elbow displayed as a sphere
    mesh_drawable elbow = mesh_primitive_sphere(0.055f);

    // Build the hierarchy:
    // Syntax to add element
    //   hierarchy.add(visual_element, element_name, parent_name, (opt)[translation, rotation])
    hierarchy.add(body, "body");
    hierarchy.add(grip, "grip_left", "body", 2 * radius_body * vec3(1 / 3.0f, 1 / 2.0f, 1 / 1.5f));
    hierarchy.add(grip, "grip_right", "body", 2 * radius_body * vec3(-1 / 3.0f, 1 / 2.0f, 1 / 1.5f));
    hierarchy.add(head, "head", "body", (radius_body + radius_head) * vec3(0, 0.43f, 0.9f));
    // Eyes positions are set with respect to some ratio of the
    hierarchy.add(eye, "eye_left", "head", radius_head * vec3(1 / 3.0f, 1 / 2.0f, 1 / 1.5f));
    hierarchy.add(eye, "eye_right", "head", radius_head * vec3(-1 / 3.0f, 1 / 2.0f, 1 / 1.5f));
    hierarchy.add(beak, "beak", "head", radius_head * vec3(0.0f, 0.0f, 0.9f));

    // Set the left part of the body arm: shoulder-elbow-arm
    hierarchy.add(shoulder, "shoulder_left", "body", { -radius_body + 0.05f,0,0 }); // extremity of the spherical body
    hierarchy.add(arm, "arm_bottom_left", "shoulder_left", { -0.2f,0,0 });                        // the arm start at the center of the elbow

    // Set the right part of the body arm: similar to the left part excepted a symmetry is applied along x direction for the shoulder
    hierarchy.add(shoulder, "shoulder_right", "body", { {radius_body - 0.05f,0,0}, {-1,0,0, 0,1,0, 0,0,1}/*Symmetry*/ });
    hierarchy.add(arm, "arm_bottom_right", "shoulder_right", { -0.2f,0,0 });


    // Set the same shader for all the elements
    hierarchy.set_shader_for_all_elements(shaders["mesh"]);

    // Initialize helper structure to display the hierarchy skeleton
    hierarchy_visual_debug.init(shaders["segment_im"], shaders["mesh"]);

    // Initial Keyframe data vector of (position, time)
    keyframes = { {  {-2.45853f,-0.208157f,-0.832888f}  , 0.0f  },
                  { {-1.34956f,-0.202637f,-2.63806f}  , 1.0f  },
                  {  {0.40397f,-0.176731f,-3.4436f} , 2.0f  },
                  {  {2.85194f,0.0610485f,-3.45427f} , 3.0f  },
                  {  {4.34616f,-0.0543537f,-1.98314f}, 4.0f  },
                  { {4.69917f,-0.270159f,-0.113098f}   , 5.0f  },
                  {  {4.27974f,-0.84472f,2.35718f}, 6.0f  },
                  {  {2.47521f,-1.20712f,3.41552f} , 7.0f  },
                  { {0.242247f,-1.01565f,3.34052f}, 8.0f  },
                  {  {-1.3442f,-0.784094f,2.58957f} , 9.0f  },
                  { {-2.17222f,-0.395376f,1.1636f}   , 10.0f },
                  { {-1.0f,1.0f,0.0f}   , 11.0f },
    };

    prev_p = 2 * keyframes[0].p - keyframes[1].p;

    // Set timer bounds
    // You should adapt these extremal values to the type of interpolation
    timer.t_min = keyframes[0].t;                   // first time of the keyframe
    timer.t_max = keyframes[keyframes.size()-1].t;  // last time of the keyframe
    timer.t = timer.t_min;
    timer.scale = 0.25;

    keyframe_visual = mesh_primitive_sphere();
    keyframe_visual.shader = shaders["mesh"];
    keyframe_visual.uniform.color = {1,1,1};
    keyframe_visual.uniform.transform.scaling = 0.05f;

    keyframe_picked = mesh_primitive_sphere();
    keyframe_picked.shader = shaders["mesh"];
    keyframe_picked.uniform.color = {1,0,0};
    keyframe_picked.uniform.transform.scaling = 0.055f;

    segment_drawer.init();

    trajectory = curve_dynamic_drawable(500); // number of steps stored in the trajectory
    trajectory.uniform.color = {0,0,1};

    picked_object=-1;
    scene.camera.scale = 7.0f;
    cloth.setup_data(shaders, scene, gui, radius_head * vec3(1 / 3.0f, 1 / 2.0f, 1 / 1.5f),
            radius_head * vec3(-1 / 3.0f, 1 / 2.0f, 1 / 1.5f));

    sphere.setup_data(shaders, scene, gui);
}




void scene_model::frame_draw(std::map<std::string,GLuint>& shaders,
        scene_structure& scene, gui_structure& gui)
{
    timer.update();
    const float t = timer.t;

    /** *************************************************************  **/
    /** Compute the (animated) transformations applied to the elements **/
    /** *************************************************************  **/

    // The body oscillate along the z direction
    // hierarchy["body"].transform.translation = {0,0,0.2f*(1+std::sin(2*3.14f*t))};

    // Rotation of the shoulder around the y axis
    mat3 const R_shoulder = rotation_from_axis_angle_mat3({0,0,1}, std::sin(2*3.14f*((t * 2.0f)-0.4f)) );
    // Rotation of the arm around the y axis (delayed with respect to the shoulder)
    mat3 const R_arm = rotation_from_axis_angle_mat3({0,0,1}, std::sin(2*3.14f*((t * 2.0f)-0.6f)) );
    mat3 const head = rotation_from_axis_angle_mat3({1,0,0}, std::sin(2*3.14f*(t * 1.0f)) * 0.5f );
    // Symmetry in the x-direction between the left/right parts
    mat3 const Symmetry = {-1,0,0, 0,1,0, 0,0,1};

    // Set the rotation to the elements in the hierarchy
    hierarchy["shoulder_left"].transform.rotation = R_shoulder;
    hierarchy["arm_bottom_left"].transform.rotation = R_arm;

    hierarchy["shoulder_right"].transform.rotation = Symmetry*R_shoulder; // apply the symmetry
    hierarchy["arm_bottom_right"].transform.rotation = R_arm; //note that the symmetry is already applied by the parent element
    hierarchy["head"].transform.rotation = head;

    // if( t<timer.t_min+0.1f ) // clear trajectory when the timer restart
    //     trajectory.clear();

    set_gui();

    // ********************************************* //
    // Compute interpolated position at time t
    // ********************************************* //
    const int idx = index_at_value(t, keyframes);
    // Assume a closed curve trajectory
    const size_t N = keyframes.size();
    int i0 = N-2;
    if (idx != 0)
        i0 = idx - 1;
    int i1 = (idx + 1) % N;
    int i2 = (idx + 2) % N;
    int i1p = i1 % (N - 1);
    int i2p = i2 % (N - 1);
    // Preparation of data for the linear interpolation
    // Parameters used to compute the linear interpolation
    float t0 = keyframes[i0].t;
    const float t1 = keyframes[idx].t; // = t_i
    float t2 = keyframes[i1].t; // = t_{i+1}
    float t3 = keyframes[i2].t;

    const vec3& p0 = keyframes[i0].p;
    const vec3& p1 = keyframes[idx].p; // = p_i
    const vec3& p2 = keyframes[i1p].p; // = p_{i+1}
    const vec3& p3 = keyframes[i2p].p;
    
    if (i1 < idx)
        t2 += timer.t_max;
    if (i2 < idx)
        t3 += timer.t_max;
    if (i0 > idx)
        t0 -= timer.t_max;


    // Compute interpolation
    const vec3 p = cardinal_spline_interpolation(t, t0, t1, t2, t3, p0, p1, p2, p3);
    const vec3 facing = p - prev_p;
    prev_p = p;
    mat3 const body = rotation_between_vector_mat3({0,0,1}, facing);
    hierarchy["body"].transform.rotation = body;



    // Store current trajectory of point p
    trajectory.add_point(p);


    // Draw current position
    hierarchy["body"].transform.translation = p;
    hierarchy.update_local_to_global_coordinates();

    // Draw moving point trajectory
    trajectory.draw(shaders["curve"], scene.camera);


    if(gui_scene.surface) // The default display
        draw(hierarchy, scene.camera);

    if(gui_scene.wireframe) // Display the hierarchy as wireframe
        draw(hierarchy, scene.camera, shaders["wireframe"]);

    if(gui_scene.skeleton) // Display the skeleton of the hierarchy (debug)
        hierarchy_visual_debug.draw(hierarchy, scene.camera);

    // Draw sphere at each keyframe position
    if(gui_scene.display_keyframe) {
        for(size_t k=0; k<N-1; ++k)
        {
            const vec3& p_keyframe = keyframes[k].p;
            keyframe_visual.uniform.transform.translation = p_keyframe;
            draw(keyframe_visual, scene.camera);
        }
    }

    // Draw selected sphere in red
    if( picked_object!=-1 )
    {
        const vec3& p_keyframe = keyframes[picked_object].p;
        keyframe_picked.uniform.transform.translation = p_keyframe;
        draw(keyframe_picked, scene.camera);
    }


    // Draw segments between each keyframe
    if(gui_scene.display_polygon) {
        for(size_t k=0; k<keyframes.size()-2; ++k)
        {
            const vec3& pa = keyframes[k].p;
            const vec3& pb = keyframes[k+1].p;

            segment_drawer.uniform_parameter.p1 = pa;
            segment_drawer.uniform_parameter.p2 = pb;
            segment_drawer.draw(shaders["segment_im"], scene.camera);
        }
        segment_drawer.uniform_parameter.p1 = keyframes[N-2].p;
        segment_drawer.uniform_parameter.p2 = keyframes[0].p;
        segment_drawer.draw(shaders["segment_im"], scene.camera);
    }

    vec3 base = hierarchy["body"].transform.translation;
    mat3 rot = hierarchy["body"].transform.rotation;
    cloth.frame_draw(shaders, scene, gui,
        base + rot * hierarchy["grip_left"].transform.translation,
        base + rot * hierarchy["grip_right"].transform.translation, -facing);

    sphere.frame_draw(shaders, scene, gui, base, -facing);
}



void scene_model::mouse_click(scene_structure& scene, GLFWwindow* window, int , int , int )
{
    // Mouse click is used to select a position of the control polygon
    // ******************************************************************** //

    // Cursor coordinates
    const vec2 cursor = glfw_cursor_coordinates_window(window);

    // Check that the mouse is clicked (drag and drop)
    const bool mouse_click_left  = glfw_mouse_pressed_left(window);
    const bool key_shift = glfw_key_shift_pressed(window);

    // Check if shift key is pressed
    if(mouse_click_left && key_shift)
    {
        // Create the 3D ray passing by the selected point on the screen
        const ray r = picking_ray(scene.camera, cursor);

        // Check if this ray intersects a position (represented by a sphere)
        //  Loop over all positions and get the intersected position (the closest one in case of multiple intersection)
        const size_t N = keyframes.size();
        picked_object = -1;
        float distance_min = 0.0f;
        for(size_t k=0; k<N; ++k)
        {
            const vec3 c = keyframes[k].p;
            const picking_info info = ray_intersect_sphere(r, c, 0.1f);

            if( info.picking_valid ) // the ray intersects a sphere
            {
                const float distance = norm(info.intersection-r.p); // get the closest intersection
                if( picked_object==-1 || distance<distance_min ){
                    distance_min = distance;
                    picked_object = k;
                }
            }
        }
    }

}

void scene_model::mouse_move(scene_structure& scene, GLFWwindow* window)
{

    const bool mouse_click_left  = glfw_mouse_pressed_left(window);
    const bool key_shift = glfw_key_shift_pressed(window);
    if(mouse_click_left && key_shift && picked_object!=-1)
    {
        // Translate the selected object to the new pointed mouse position within the camera plane
        // ************************************************************************************** //

        // Get vector orthogonal to camera orientation
        const mat4 M = scene.camera.camera_matrix();
        const vec3 n = {M(0,2),M(1,2),M(2,2)};

        // Compute intersection between current ray and the plane orthogonal to the view direction and passing by the selected object
        const vec2 cursor = glfw_cursor_coordinates_window(window);
        const ray r = picking_ray(scene.camera, cursor);
        vec3& p0 = keyframes[picked_object].p;
        const picking_info info = ray_intersect_plane(r,n,p0);

        // translate the position
        p0 = info.intersection;

    }
}

void scene_model::set_gui()
{
    ImGui::SliderFloat("Time", &timer.t, timer.t_min, timer.t_max);
    ImGui::SliderFloat("Time scale", &timer.scale, 0.1f, 3.0f);

    ImGui::Text("Display: "); ImGui::SameLine();
    ImGui::Checkbox("keyframe", &gui_scene.display_keyframe); ImGui::SameLine();
    ImGui::Checkbox("polygon", &gui_scene.display_polygon);

    if( ImGui::Button("Print Keyframe") )
    {
        std::cout<<"keyframe_position={";
        for(size_t k=0; k<keyframes.size(); ++k)
        {
            const vec3& p = keyframes[k].p;
            std::cout<< "{"<<p.x<<"f,"<<p.y<<"f,"<<p.z<<"f}";
            if(k<keyframes.size()-1)
                std::cout<<", ";
        }
        std::cout<<"}"<<std::endl;
    }

}



static size_t index_at_value(float t, vcl::buffer<vec3t> const& v)
{
    const size_t N = v.size();
    assert(v.size()>=2);
    assert(t>=v[0].t);
    assert(t<v[N-1].t);

    size_t k=0;
    while( v[k+1].t<t )
        ++k;
    return k;
}


static vec3 linear_interpolation(float t, float t1, float t2, const vec3& p1, const vec3& p2)
{
    const float alpha = (t-t1)/(t2-t1);
    const vec3 p = (1-alpha)*p1 + alpha*p2;
    return p;
}

vec3 cardinal_spline_interpolation(float t, float t0, float t1, float t2, float t3, const vec3& p0, const vec3& p1, const vec3& p2, const vec3& p3) {
    float mu = 1;
    float s = (t - t1) / (t2 - t1);
    float s2 = s * s;
    float s3 = s * s * s;
    vec3 d1 = mu * ((p2 - p0) / (t2 - t0));
    vec3 d2 = mu * ((p3 - p1) / (t3 - t1));
    return (2 * s3 - 3 * s2 + 1) * p1 + (s3 - 2 * s2 + s) * d1 + (-2 * s3 + 3 * s2) * p2 + (s3 - s2) * d2;
}

#endif

