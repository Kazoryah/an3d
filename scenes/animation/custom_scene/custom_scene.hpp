#pragma once

#include "main/scene_base/base.hpp"
#include "./cloth.hpp"
#include "./sphere_collision.hpp"

#ifdef CUSTOM_SCENE

// Store a vec3 (p) + time (t)
struct vec3t{
    vcl::vec3 p; // position
    float t;     // time
};


struct scene_model : scene_base
{
    void setup_data(std::map<std::string,GLuint>& shaders, scene_structure& scene, gui_structure& gui);
    void frame_draw(std::map<std::string,GLuint>& shaders, scene_structure& scene, gui_structure& gui);

    void set_gui();

    // Called every time the mouse is clicked
    void mouse_click(scene_structure& scene, GLFWwindow* window, int button, int action, int mods);
    // Called every time the mouse is moved
    void mouse_move(scene_structure& scene, GLFWwindow* window);

    // Data (p_i,t_i)
    vcl::buffer<vec3t> keyframes; // Given (position,time)

    vcl::hierarchy_mesh_drawable hierarchy;
    vcl::vec3 prev_p;
    vcl::hierarchy_mesh_drawable_display_skeleton hierarchy_visual_debug;
    vcl::mesh_drawable keyframe_visual;                    // keyframe samples
    vcl::mesh_drawable keyframe_picked;                    // showing the picked sample
    vcl::segment_drawable_immediate_mode segment_drawer;   // used to draw segments between keyframe samples
    vcl::curve_dynamic_drawable trajectory;                // Draw the trajectory of the moving point as a curve

    vcl::mesh_drawable ground;
    // Store the index of a selected sphere
    int picked_object;

    gui_scene_structure gui_scene;
    vcl::timer_interval timer;

    cloth_scene_model cloth;
    sphere_scene_model sphere;
};

#endif
