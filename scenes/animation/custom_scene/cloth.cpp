
#include "cloth.hpp"
#include <algorithm>


#ifdef CUSTOM_SCENE

using namespace vcl;


// Fill value of force applied on each particle
// - Gravity
// - Drag
// - Spring force
// - Wind force
void cloth_scene_model::compute_forces(vec3 vwind)
{
    const size_t N = force.size();        // Total number of particles of the cloth Nu x Nv
    const int N_dim = int(force.dimension[0]); // Number of particles along one dimension (square dimension)

    simulation_parameters.m = user_parameters.m / float(N); // Constant total mass

    // Get simuation parameters
    const float K  = user_parameters.K;
    const float m  = simulation_parameters.m;
    const float L0 = simulation_parameters.L0;
    const float wind = user_parameters.wind;

    // Gravity
    const vec3 g = {0,-9.81f,0};
    for(size_t k=0; k<N; ++k)
        force[k] = m * g;

    // Drag
    const float mu = user_parameters.mu;
    for(size_t k=0; k<N; ++k)
        force[k] = force[k] - mu * speed[k];

    // Springs
    for(int ku=0; ku<N_dim; ++ku) {
      int t_border = std::max(0, ku - 2);
      if (ku - 2 < 0)
        t_border = std::max(0, ku - 1);
      int b_border = std::min(N_dim - 1, ku + 2);
      if (ku + 2 > N_dim - 1)
        t_border = std::max(0, ku - 1);
      for(int kv=0; kv<N_dim; ++kv) {
          // To do ...
          // Compute spring forces force(ku,kv) = ...
          int l_border = std::max(0, kv - 2);
          if (kv - 2 < 0)
            l_border = std::max(0, kv - 1);

          int r_border = std::min(N_dim - 1, kv + 2);
          if (kv + 2 > N_dim - 1)
              r_border = std::min(N_dim - 1, kv + 1);

          vcl::vec3 force_i = {0, 0, 0};
          vcl::vec3 p_i = position[ku * N_dim + kv];

          for (int i = t_border; i <= b_border; i++)
          {
              for (int j = l_border; j <= r_border; j++)
              {
                  if (i == ku && j == kv)
                      continue;
                  float K_ij = K;
                  float L0_ij = L0;
                  if ((i == ku - 1 && j == kv - 1) || (i == ku + 1 && j == kv + 1)
                      || (i == ku + 1 && j == kv - 1) || (i == ku - 1 && j == kv + 1))
                      L0_ij = std::sqrt(L0 * L0 * 2);

                  if ((i == ku - 2 && j == kv - 2) || (i == ku + 2 && j == kv + 2)
                      || (i == ku + 2 && j == kv - 2) || (i == ku - 2 && j == kv + 2))
                      continue;

                  // Weird stuff happening with bending springs, so they are
                  // removed for now
                  if (j == kv - 2 || i == ku + 2 || i == ku - 2 || j == kv + 2)
                      K_ij = 0;

                  vcl::vec3 p_j = position[i * N_dim + j];

                  vcl::vec3 vec_ij = p_j - p_i;
                  float norm_ij = norm(vec_ij);

                  force_i += K_ij * (norm_ij - L0_ij) * vec_ij / norm_ij;
              }
          }
          vec3 normal_i = normals.data[ku * N_dim + kv];
          float wind_i = wind * dot(normal_i, vwind * 100);

          force[ku * N_dim + kv] = force[ku * N_dim + kv] + force_i + wind_i * normal_i;
      }
    }
}


// Handle detection and response to collision with the shape described in "collision_shapes" variable
void cloth_scene_model::collision_constraints()
{
    // Handle collisions here (with the ground and the sphere)
    // ...


}



// Initialize the geometrical model
void cloth_scene_model::initialize(vec3 first_constraint, vec3 second_constraint)
{
    // Number of samples of the model (total number of particles is N_cloth x N_cloth)
    const size_t N_cloth = 50;

    // Rest length (length of an edge)
    float length = norm(first_constraint - second_constraint);
    simulation_parameters.L0 = 1 / float(N_cloth - 1);

    // Create cloth mesh in its initial position
    // Horizontal grid of length 1 x 1
    const mesh base_cloth = mesh_primitive_grid(N_cloth ,N_cloth, first_constraint,
                                                                {0.1f, 0, 0},
                                                                {0, 0, 0.1f});

    // Set particle position from cloth geometry
    position = buffer2D_from_vector(base_cloth.position, N_cloth, N_cloth);

    // Set hard positional constraints
    positional_constraints[0] = position[0];
    positional_constraints[N_cloth*(N_cloth-1)] = position[N_cloth*(N_cloth-1)];

    // Init particles data (speed, force)
    speed.resize(position.dimension); speed.fill({0,0,0});
    force.resize(position.dimension); force.fill({0,0,0});


    // Store connectivity and normals
    connectivity = base_cloth.connectivity;
    normals      = normal(position.data,connectivity);

    // Send data to GPU
    cloth.clear();
    cloth = mesh_drawable(base_cloth);
    cloth.uniform.shading.specular = 0.0f;
    cloth.shader = shader_mesh;
    cloth.texture_id = texture_cloth;

    simulation_diverged = false;
    force_simulation    = false;

    timer.update();
}

void cloth_scene_model::setup_data(std::map<std::string,GLuint>& shaders,
        scene_structure& , gui_structure& gui, vec3 first_constraint, vec3
        second_constraint)
{
    gui.show_frame_camera = false;

    // Load textures
    texture_cloth = create_texture_gpu(image_load_png("scenes/animation/02_simulation/assets/cloth.png"));
    //texture_wood  = create_texture_gpu(image_load_png("scenes/animation/02_simulation/assets/wood.png"));
    shader_mesh = shaders["mesh_bf"];

    // Initialize cloth geometry and particles
    initialize(first_constraint, second_constraint);

    // Default value for simulation parameters
    user_parameters.K    = 100.0f;
    user_parameters.m    = 5.0f;
    user_parameters.wind = 0.0f;
    user_parameters.mu   = 0.02f;

    // Set collision shapes
    //collision_shapes.sphere_p = {0,0.1f,0};
    //collision_shapes.sphere_r = 0.2f;
    //collision_shapes.ground_height = 0.1f;

    // Init visual models
    //sphere = mesh_drawable(mesh_primitive_sphere(1.0f,{0,0,0},60,60));
    //sphere.shader = shaders["mesh"];
    //sphere.uniform.color = {1,1,0};

    //ground = mesh_drawable(mesh_primitive_quad({-1,collision_shapes.ground_height-1e-3f,-1}, {1,collision_shapes.ground_height-1e-3f,-1}, {1,collision_shapes.ground_height-1e-3f,1}, {-1,collision_shapes.ground_height-1e-3f,1}));
    //ground.shader = shaders["mesh_bf"];
    //ground.texture_id = texture_wood;

    gui_display_texture = true;
    gui_display_wireframe = false;
}

void cloth_scene_model::frame_draw(std::map<std::string,GLuint>& shaders,
        scene_structure& scene, gui_structure& gui, vec3 first_constraint, vec3
        second_constraint, vec3 vwind)
{
    const float dt = timer.update();
    set_gui(first_constraint, second_constraint);

    // Force constant simulation time step
    float h = dt<=1e-6f? 0.0f : timer.scale*0.001f;

    if( (!simulation_diverged || force_simulation) && h>0)
    {
        // Iterate over a fixed number of substeps between each frames
        const size_t number_of_substeps = 4;
        for(size_t k=0; (!simulation_diverged  || force_simulation) && k<number_of_substeps; ++k)
        {
            compute_forces(vwind);
            numerical_integration(h);
            collision_constraints();                 // Detect and solve collision with other shapes

            hard_constraints(first_constraint, second_constraint);                      // Enforce hard positional constraints

            normal(position.data, connectivity, normals); // Update normals of the cloth
            detect_simulation_divergence();               // Check if the simulation seems to diverge
        }
    }


    cloth.update_position(position.data);
    cloth.update_normal(normals.data);

    display_elements(shaders, scene, gui);

}

void cloth_scene_model::numerical_integration(float h)
{
    const size_t NN = position.size();
    const float m = simulation_parameters.m;

    for(size_t k=0; k<NN; ++k)
    {
        vec3& p = position[k];
        vec3& v = speed[k];
        const vec3& f = force[k];

        v = v + h*f/m;
        p = p + h*v;
    }
}

void cloth_scene_model::hard_constraints(vec3 first_constraint, vec3 second_constraint)
{
    // Fixed positions of the cloth
    for(const auto& constraints : positional_constraints)
    {
        if (constraints.first == 0)
            position[constraints.first] = first_constraint;
        else
            position[constraints.first] = second_constraint;
    }
}

void cloth_scene_model::display_elements(std::map<std::string,GLuint>& shaders, scene_structure& scene, gui_structure& )
{
    glEnable( GL_POLYGON_OFFSET_FILL );

    // Display cloth
    GLuint texture = cloth.texture_id;
    if(!gui_display_texture)
        texture = scene.texture_white;

    glPolygonOffset( 1.0, 1.0 );
    draw(cloth, scene.camera, cloth.shader, texture);
    glBindTexture(GL_TEXTURE_2D, scene.texture_white);

    if(gui_display_wireframe) {
        glPolygonOffset( 1.0, 1.0 );
        draw(cloth, scene.camera, shaders["wireframe_quads"]);
    }


    // Display positional constraint using spheres
    //sphere.uniform.transform.scaling = 0.02f;
    //for(const auto& constraints : positional_constraints)  {
    //    sphere.uniform.transform.translation = constraints.second;
    //    draw(sphere, scene.camera, shaders["mesh"]);
    //}


    // Display sphere used for collision
    //sphere.uniform.transform.scaling     = collision_shapes.sphere_r;
    //sphere.uniform.transform.translation = collision_shapes.sphere_p;
    //draw(sphere, scene.camera, shaders["mesh"]);

    // Display ground
    //draw(ground, scene.camera);
    //glBindTexture(GL_TEXTURE_2D, scene.texture_white);
}


// Automatic detection of divergence: stop the simulation if detected
void cloth_scene_model::detect_simulation_divergence()
{
    const size_t NN = position.size();
    for(size_t k=0; simulation_diverged==false && k<NN; ++k)
    {
        const float f = norm(force[k]);
        const vec3& p = position[k];

        if( std::isnan(f) ) // detect NaN in force
        {
            std::cout<<"NaN detected in forces"<<std::endl;
            simulation_diverged = true;
        }

        if( f>1000.0f ) // detect strong force magnitude
        {
            std::cout<<" **** Warning : Strong force magnitude detected "<<f<<" at vertex "<<k<<" ****"<<std::endl;
            simulation_diverged = true;
        }

        if( std::isnan(p.x) || std::isnan(p.y) || std::isnan(p.z) ) // detect NaN in position
        {
            std::cout<<"NaN detected in positions"<<std::endl;
            simulation_diverged = true;
        }

        if(simulation_diverged==true)
        {
            std::cerr<<" **** Simulation has diverged **** "<<std::endl;
            std::cerr<<" > Stop simulation iterations"<<std::endl;
            timer.stop();
        }
    }

}


void cloth_scene_model::set_gui(vec3 first_constraint, vec3 second_constraint)
{
    ImGui::SliderFloat("Time scale", &timer.scale, 0.05f, 2.0f, "%.2f s");
    ImGui::SliderFloat("Stiffness", &user_parameters.K, 1.0f, 1000.0f, "%.2f s");
    ImGui::SliderFloat("Damping", &user_parameters.mu, 0.0f, 0.1f, "%.3f s");
    ImGui::SliderFloat("Mass", &user_parameters.m, 1.0f, 15.0f, "%.2f s");
    ImGui::SliderFloat("Wind", &user_parameters.wind, 0.0f, 1.0f, "%.2f s");

    ImGui::Checkbox("Wireframe",&gui_display_wireframe);
    ImGui::Checkbox("Texture",&gui_display_texture);

    bool const stop  = ImGui::Button("Stop anim"); ImGui::SameLine();
    bool const start = ImGui::Button("Start anim");

    if(stop)  timer.stop();
    if(start) {
        if( simulation_diverged )
            force_simulation=true;
        timer.start();
    }

    bool const restart = ImGui::Button("Restart");
    if(restart) initialize(first_constraint, second_constraint);
}

#endif
